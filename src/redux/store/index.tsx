import {combineReducers} from 'redux';
import { userReducer } from '../reducer/userReducer';

const stores = () => combineReducers({
    user: userReducer
});
export default stores
