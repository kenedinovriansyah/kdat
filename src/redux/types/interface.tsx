import {Moment} from 'moment';

export interface User {
  id?: string;
  username?: string;
  email?: string;
  first_name?: string;
  last_name?: string;
  password?: string;
  confirm_password?: string;
  date_joined?: Moment;
}

export interface Authenticate {
  id?: number;
  public_id?: string;

  avatar?: string;
  phone_number?: string;
  country?: string;
  states?: string;
  city?: string;
  user?: User;
}

type Color = 'red' | 'green';

export interface Message {
  message?: any;
  color?: Color;
  active: boolean;
}
