import {Authenticate, User, Message} from './interface';

export enum UserTypes {
  SignIn = 'SignIn',
  SignOut = 'SignOut',
  SignUp = 'SignUp',
  Me = 'Me',
  Failured = "Failured"
}

export interface UserState {
  readonly user: User;
  readonly author: Authenticate;
  readonly all_author: Authenticate[];
  readonly all_user: User[];
  readonly message: Message;
  readonly token: string;
}
