import axios, { AxiosResponse } from 'axios'
import { Dispatch } from 'redux'
import { network } from '../../network'
import { User } from '../types/interface'
import { UserTypes } from '../types/userTypes'

export const userAccess = (data: User) => {
    return async (dispatch: Dispatch) => {
        const response = await axios.post(network + "/api-token-auth/", data, {
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': network,
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Headers': 'Content-Type, Origin, Accpeted, X-Requested-With',
            },
            timeout: 865000,
            responseType: 'json',
            withCredentials: false,
            maxContentLength: 2000,
            maxRedirects: 5,
            validateStatus: (status: number) => status >= 200 && status < 300
        }).then((res: AxiosResponse) => {
            dispatch({
                type: UserTypes.SignIn,
                payload: {
                    token: res.data.token
                }
            })
        }).catch((err) => {
            dispatch({
                type: UserTypes.Failured,
                payload: {
                    message: {
                        message: err.response.data,
                        color: 'red',
                        active: true
                    }
                }
            })
        })
        return response
    }
}