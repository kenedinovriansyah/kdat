import {applyMiddleware, createStore, Store} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';
import loggerMiddleware from 'redux-logger';
import store_ from '../store';
import {UserState} from '../types/userTypes';

export interface ApplicationState {
  user: UserState;
}

export default function configureStore(preloadedState?: any) {
  const middleware = [thunkMiddleware];
  const store: Store<ApplicationState> = createStore(
    store_(),
    preloadedState,
    composeWithDevTools(applyMiddleware(...middleware, loggerMiddleware)),
  );
  if (module.hot) {
    module.hot.accept(() => {
      store.replaceReducer(store_());
    });
  }
  return store;
}
