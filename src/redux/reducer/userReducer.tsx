import {Reducer} from 'redux';
import {UserState, UserTypes} from '../types/userTypes';

const initialState: UserState = {
  token: '',
  user: {},
  author: {},
  all_author: [],
  all_user: [],
  message: {
    active: false,
  },
};

export const userReducer: Reducer<UserState> = (
  state = initialState,
  action,
) => {
  switch (action.type) {
    case UserTypes.SignIn:
      return {
        ...state,
        token: action.payload.token,
      };
      break;
    case UserTypes.SignUp:
      return {
        ...state,
        message: action.payload.message,
      };
      break;
    case UserTypes.Failured:
      return {
        ...state,
        message: action.payload.message,
      };
      break;
    default:
      return state;
      break;
  }
};
