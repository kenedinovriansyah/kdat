import { Text, View } from 'native-base'
import React from 'react'
import { SafeAreaView } from 'react-native'

const TemplateComponent: React.FC = () => {
    return (
        <SafeAreaView>
            <View>
                <Text>
                    Template
                </Text>
            </View>
        </SafeAreaView>
    )
}

export default TemplateComponent