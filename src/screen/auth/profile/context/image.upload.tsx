import {Text, View} from 'native-base';
import React from 'react';
import {
  GestureResponderEvent,
  PermissionsAndroid,
  Platform,
  TouchableOpacity,
} from 'react-native';
import Modal from 'react-native-modal';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker/src';
import {AuthInter} from '../../types/interfaceUser';

interface ContextProps {
  imageUpload: boolean;
  setImageUpload: React.Dispatch<React.SetStateAction<boolean>>;
  handleClickChangeAvatar?(args: GestureResponderEvent): void;
  state: AuthInter;
  setState: React.Dispatch<React.SetStateAction<AuthInter>>;
}

export const ImageUpload = React.createContext<ContextProps>({});

export const ImageUploadApp = () => {
  const {setImageUpload, state, setState} = React.useContext(ImageUpload);
  const PermissionCamera = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera permissions',
            message: 'Apps need camera permissions',
            buttonPositive: '',
          },
        );
        return granted && PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        return false;
      }
    } else return true;
  };
  const PermissionWriteExtenal = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Write extenal permission',
            message: 'Apps need Write extenal permission',
            buttonPositive: '',
          },
        );
        return granted && PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        return false;
      }
    } else return true;
  };
  const handleClickLibrary = () => {
    setImageUpload(false);
    launchImageLibrary(
      {
        mediaType: 'photo',
        maxHeight: 120,
        maxWidth: 120,
        quality: 1,
      },
      (res) => {
        setState({
          ...state,
          avatar: res,
        });
      },
    );
  };
  const handleClickCamera = () => {
    setImageUpload(false);
    const permissionC = PermissionCamera();
    const permissionW = PermissionWriteExtenal();
    if (permissionC && permissionW) {
      launchCamera(
        {
          mediaType: 'photo',
          maxWidth: 120,
          maxHeight: 120,
          quality: 1,
          saveToPhotos: true,
        },
        (res) => {
          setState({
            ...state,
            avatar: res,
          });
        },
      );
    }
  };
  return (
    <ImageUpload.Consumer>
      {({imageUpload, handleClickChangeAvatar}) => {
        return (
          <Modal isVisible={imageUpload}>
            <View
              style={{
                backgroundColor: 'white',
                elevation: 4,
                height: 200,
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column',
              }}>
              <TouchableOpacity
                style={{
                  backgroundColor: '#57c292',
                  width: '60%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingTop: 10,
                  paddingBottom: 10,
                  borderRadius: 5,
                  marginBottom: 5,
                  elevation: 4,
                }}
                onPress={handleClickLibrary}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: 17,
                    fontWeight: 'bold',
                  }}>
                  Open Image
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: '#57c292',
                  width: '60%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingTop: 10,
                  paddingBottom: 10,
                  borderRadius: 5,
                  marginBottom: 5,
                  elevation: 4,
                }}
                onPress={handleClickCamera}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: 17,
                    fontWeight: 'bold',
                  }}>
                  Open Camera
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: 'transparent',
                  width: '60%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingTop: 10,
                  paddingBottom: 10,
                  borderRadius: 5,
                  marginBottom: 5,
                }} onPress={handleClickChangeAvatar}>
                <Text
                  style={{
                    color: 'black',
                    fontSize: 17,
                    fontWeight: 'bold',
                  }}>
                  Cancel
                </Text>
              </TouchableOpacity>
            </View>
          </Modal>
        );
      }}
    </ImageUpload.Consumer>
  );
};
