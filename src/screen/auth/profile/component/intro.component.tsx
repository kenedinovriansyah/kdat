import {Text, View} from 'native-base';
import React from 'react';
import {FlatList} from 'react-native';
import Svg from 'react-native-svg';
import GlobeSvg from '../../../../assets/svg/germany.svg';
import PuzzleSvg from '../../../../assets/svg/puzzle.svg';
import HealthSvg from '../../../../assets/svg/daily-health-app.svg';
import SchoolSvg from '../../../../assets/svg/university.svg'
import CompanySvg from '../../../../assets/svg/business-and-trade.svg'
import JobSvg from '../../../../assets/svg/suitcase.svg'
import {SplitTemplate} from './class/intro.class';

export interface ItemsIntro {
  name: string;
  context: string;
  icon: React.ReactElement<Svg>;
}

interface FlatListState {
  index: number;
  item: ItemsIntro;
}

const CustomArray: ItemsIntro[] = [
  {
    name: 'Status',
    context: 'Lajang',
    icon: <HealthSvg width={25} height={25} fill="black" />,
  },
  {
    name: 'Country',
    context: 'Cikini Jakarta Timur, Indonesia',
    icon: <GlobeSvg width={25} height={25} fill="black" />,
  },
  {
    name: 'Hobby',
    context: '#Introvert #Drink #Fotball #Cat #Dog #Mobile',
    icon: <PuzzleSvg width={25} height={25} fill="black" />,
  },
  {
      name: 'School',
      context: 'University Indonesia',
      icon: <SchoolSvg width={25} height={25} fill="black"/>
  },
  {
      name: 'Company',
      context: 'PT. Frisedea',
      icon: <CompanySvg width={25} height={25} fill="black"/>
  },
  {
      name: 'Job',
      context: 'Devops',
      icon: <JobSvg width={25} height={25} fill="black"/>
  }
];

const SplitsClass = new SplitTemplate();

const IntroComponent: React.FC = () => {
  function renderItem({index, item}: FlatListState) {
    return (
      <View
        style={{
          flexDirection: 'row',
          marginBottom: 5,
        }}
        key={index}>
        <View
          style={{
            width: 40,
            height: 40,
            backgroundColor: 'white',
            justifyContent: 'center',
            alignItems: 'center',
            elevation: 4,
            borderRadius: 10,
          }}>
          {item.icon}
        </View>
        <View
          style={{
            flexDirection: 'column',
            marginLeft: 10,
          }}>
          <Text
            style={{
              fontWeight: 'bold',
            }}>
            {item.name}
          </Text>
          {item.context.indexOf('#') === 0 ? (
            SplitsClass.constructors(item)
          ) : (
            <Text>{item.context}</Text>
          )}
        </View>
      </View>
    );
  }
  return (
    <View
      style={{
        paddingBottom: 10,
        backgroundColor: 'white',
        paddingLeft: 10,
        paddingRight: 10,
      }}>
      <FlatList
        contentContainerStyle={{
        }}
        data={CustomArray}
        renderItem={renderItem}
        keyExtractor={(item) => item.name}
      />
    </View>
  );
};

export default IntroComponent;
