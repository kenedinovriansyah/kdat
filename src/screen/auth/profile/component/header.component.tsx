import {View} from 'native-base';
import React from 'react';
import {
  GestureResponderEvent,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {AuthInter} from '../../types/interfaceUser';

interface HeaderState {
  state: AuthInter;
  handleClickChangeAvatar(args: GestureResponderEvent): void;
}

const HeaderComponent: React.FC<HeaderState> = ({
  state,
  handleClickChangeAvatar,
}: React.PropsWithChildren<HeaderState>) => {
  return (
    <View
      style={{
        height: 200,
      }}>
      <ImageBackground
        source={{uri: 'https://c.stocksy.com/a/EqZ800/z9/2044402.jpg'}}
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}
        resizeMode="cover">
        <TouchableOpacity
          style={{
            width: 80,
            height: 80,
            backgroundColor: 'white',
            elevation: 4,
            borderRadius: 10,
          }}
          onPress={handleClickChangeAvatar}>
          <Image
            style={{
              flex: 1,
              borderRadius: 10,
            }}
            source={{
              uri: state.avatar
                ? state.avatar.uri
                : 'https://sites.google.com/site/doraemon1161104319/_/rsrc/1518075394469/characters-2/nobi-nobita/Sitting-Image-Of-Nobita.png?height=200&width=188',
            }}
          />
        </TouchableOpacity>
      </ImageBackground>
    </View>
  );
};

export default HeaderComponent;
