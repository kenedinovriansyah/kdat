import React from 'react';
import {Text, View} from 'native-base';
import {Dimensions, TouchableOpacity} from 'react-native';
import {ItemsIntro} from '../intro.component';

export class SplitTemplate {
  constructors(item: ItemsIntro) {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          flexWrap: 'wrap',
          width: Dimensions.get('screen').width / 1.4
        }}>
        {item.context.split('#').map((base, indexs) => {
          return base ? (
            <TouchableOpacity
              key={indexs}
              style={{
                paddingTop: 5,
                paddingBottom: 5,
                paddingLeft: 10,
                paddingRight: 10,
                backgroundColor: '#57c292',
                margin: 1,
                borderRadius: 10,
              }}>
              <Text
                style={{
                  color: 'white',
                  fontWeight: 'bold',
                  fontSize: 14
                }}>
                {base}
              </Text>
            </TouchableOpacity>
          ) : null;
        })}
      </View>
    );
  }
}
