import React from 'react';
import {Dimensions, FlatList, Image, TouchableOpacity} from 'react-native';
import {ScreenProfile} from '../..';
import {NavigationProp} from '../../../../../routes';
import IntroComponent from '../intro.component';

interface ContextProps {
  s: ScreenProfile;
  navigation: NavigationProp;
}

export const SwitchContext = React.createContext<ContextProps>({});
export const SwitchContextApp = () => {
  return (
    <SwitchContext.Consumer>
      {({s, navigation}) => {
        if (s === 'Intro') {
          return <IntroComponent />;
        } else if (s === 'Galery') {
          return <Galery {...navigation} />;
        }
      }}
    </SwitchContext.Consumer>
  );
};

const ImageArray: any[] = [];

for (let i = 0; i < 30; i++) {
  const image =
    'https://akcdn.detik.net.id/visual/2020/01/30/63664a2c-3667-470f-b39a-a2b4d05f608d_169.png?w=650';
  const anotherImage =
    'https://media.skyegrid.id/wp-content/uploads/2020/07/one-piece-finale-1177646-1280x0-1.jpg';
  if (i % 2) {
    ImageArray.push(image);
  } else {
    ImageArray.push(anotherImage);
  }
}

interface FlatListState {
  index: number;
  item: string;
}

const Galery: React.FC<NavigationProp> = ({
  navigation,
}: React.PropsWithChildren<NavigationProp>) => {
  const handleClickNavigation = (args: string) => {
    navigation.navigate('detailImage', {
      uri: args,
    });
  };
  function renderItem({index, item}: FlatListState) {
    return (
      <TouchableOpacity
        style={{
          width: Dimensions.get('screen').width / 4.13,
          height: Dimensions.get('screen').height / 8.2,
          margin: 1.5,
          elevation: 4,
        }}
        onPress={handleClickNavigation.bind(item, item)}
        key={index}>
        <Image
          source={{uri: item}}
          style={{
            flex: 1,
            borderRadius: 10,
          }}
          resizeMode="cover"
        />
      </TouchableOpacity>
    );
  }
  return (
    <FlatList
      contentContainerStyle={{
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
      }}
      data={ImageArray}
      renderItem={renderItem}
      keyExtractor={(item) => item}
    />
  );
};
