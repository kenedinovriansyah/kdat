import {Text, View} from 'native-base';
import React from 'react';
import {Dimensions, TouchableOpacity} from 'react-native';
import SexSvg from '../../../../assets/svg/sex.svg';
import SettingsSvg from '../../../../assets/svg/adjust.svg';
import LikeSvg from '../../../../assets/svg/like.svg';
import {ScreenProfile} from '..';

interface AuthState {
  handleClickSwitchScreen(args: ScreenProfile): void;
}

const AuthComponent: React.FC<AuthState> = ({
  handleClickSwitchScreen,
}: React.PropsWithChildren<AuthState>) => {
  return (
    <View
      style={{
        backgroundColor: 'white',
        paddingBottom: 10,
      }}>
      <View
        style={{
          backgroundColor: 'white',
          height: 55,
        }}>
        <TouchableOpacity
          style={{
            position: 'absolute',
            right: 5,
            zIndex: 3,
            backgroundColor: '#57c292',
            elevation: 10,
            top: -20,
            width: 38,
            height: 38,
            borderRadius: 10,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <SettingsSvg width={20} height={20} fill="white" />
        </TouchableOpacity>
        <View
          style={{
            width: Dimensions.get('screen').width / 1.1,
            alignSelf: 'center',
            height: 100,
            backgroundColor: 'white',
            borderRadius: 10,
            elevation: 6,
            position: 'absolute',
            top: -50,
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column',
            zIndex: 0,
          }}>
          <TouchableOpacity
            style={{
              marginBottom: 10,
            }}>
            <Text
              style={{
                fontSize: 16,
              }}>
              Username
            </Text>
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: 35,
                height: 35,
                backgroundColor: 'white',
                justifyContent: 'center',
                alignItems: 'center',
                elevation: 4,
                borderRadius: 10,
              }}>
              <SexSvg width={20} height={20} fill="black" />
            </View>
            <Text
              style={{
                marginLeft: 10,
                fontWeight: 'bold',
                fontSize: 15,
              }}>
              Male | 20th
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'white',
        }}>
        <TouchableOpacity
          style={{
            backgroundColor: 'transparent',
            width: 100,
            justifyContent: 'center',
            alignItems: 'center',
            padding: 10,
            marginRight: 5,
            borderRadius: 10,
          }}
          onPress={handleClickSwitchScreen.bind('', 'Intro')}>
          <Text
            style={{
              fontSize: 15,
              fontWeight: 'bold',
              textTransform: 'capitalize',
              color: 'green',
            }}>
            Intro
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            backgroundColor: 'white',
            width: 50,
            height: 50,
            elevation: 4,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 10,
          }}>
          <LikeSvg width={30} height={30} />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            backgroundColor: 'transparent',
            width: 100,
            justifyContent: 'center',
            alignItems: 'center',
            padding: 10,
            marginLeft: 5,
            borderRadius: 10,
          }}
          onPress={handleClickSwitchScreen.bind('', 'Galery')}>
          <Text
            style={{
              fontSize: 15,
              fontWeight: 'bold',
              textTransform: 'capitalize',
              color: 'green',
            }}>
            Galery
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default AuthComponent;
