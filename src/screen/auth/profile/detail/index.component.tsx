import {Text, View} from 'native-base';
import React from 'react';
import {
  Dimensions,
  GestureResponderEvent,
  Image,
  TouchableOpacity,
} from 'react-native';
import {NavigationProp} from '../../../../routes';
import ArrowBack from '../../../../assets/svg/left-arrow.svg';
import StarSvg from '../../../../assets/svg/star.svg';
import StarAwesomeSvg from '../../../../assets/svg/star (1).svg';
import {ModalContextLikes, ModalContextLikesApp} from './modalLikes.component';

const accountsArray = [
  'example@gmail.com',
  'another@gmail.com',
  'single@gmail.com',
  'coco@gmail.com',
];

const DetailImage: React.FC<NavigationProp> = ({
  navigation,
  route,
}: React.PropsWithChildren<NavigationProp>) => {
  const [modal, setModal] = React.useState<boolean>(false);
  const [likes, setLikes] = React.useState<boolean>(false);
  const handleClickBack = (args: GestureResponderEvent) => {
    args.preventDefault();
    navigation.goBack();
  };
  const handleClickLike = (args: string) => {
    setLikes(!likes);
  };
  const handleClickModal = (args: GestureResponderEvent) => {
    setModal(!modal);
  };
  return (
    <ModalContextLikes.Provider
      value={{
        modal,
        handleClickModal,
      }}>
      <View
        style={{
          flex: 1,
        }}>
        <Image
          source={{uri: route.params.uri}}
          style={{flex: 1}}
          resizeMode="cover"
        />
        <View
          style={{
            position: 'absolute',
            bottom: 0,
            width: '100%',
            backgroundColor: 'transparent',
            height: 100,
            paddingLeft: 10,
            paddingRight: 10,
            flexDirection: 'column',
            alignItems: 'center',
          }}>
          <View
            style={{
              width: '100%',
              backgroundColor: 'transparent',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              style={{
                backgroundColor: 'black',
                width: 100,
                height: 40,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
                elevation: 4,
                flexDirection: 'row',
              }}
              onPress={handleClickBack}>
              <ArrowBack width={20} height={20} fill="white" />
              <Text
                style={{
                  color: 'white',
                  fontSize: 17,
                  fontWeight: 'bold',
                  marginLeft: 10,
                }}>
                Back
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                marginLeft: 20,
                backgroundColor: 'white',
                paddingLeft: 15,
                paddingRight: 15,
                height: 40,
                borderRadius: 10,
                elevation: 4,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              }}
              onPress={handleClickLike.bind('', '')}>
              {likes ? (
                <StarAwesomeSvg width={25} height={25} />
              ) : (
                <StarSvg width={25} height={25} />
              )}
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginLeft: 10,
                  color: 'red',
                }}>
                Likes
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: '100%',
              backgroundColor: 'white',
              padding: 10,
              marginTop: 8,
              maxHeight: 80,
              elevation: 4,
              borderRadius: 10,
              flexDirection: 'row',
              alignItems: 'center',
              overflow: 'hidden',
            }}>
            <Text>Likes from</Text>
            <TouchableOpacity>
              <Text
                numberOfLines={1}
                style={{
                  fontWeight: 'bold',
                  marginLeft: 4,
                  width: Dimensions.get('screen').width / 1.5,
                }}
                onPress={handleClickModal}>
                {accountsArray.map((base, index) => (
                  <Text
                    key={index}
                    style={{
                      marginLeft: 4,
                    }}>
                    {base}
                  </Text>
                ))}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <ModalContextLikesApp />
    </ModalContextLikes.Provider>
  );
};

export default DetailImage;
