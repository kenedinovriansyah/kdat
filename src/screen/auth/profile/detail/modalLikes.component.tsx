import {Text, View} from 'native-base';
import React from 'react';
import {
  FlatList,
  GestureResponderEvent,
  Image,
  TouchableOpacity,
} from 'react-native';
import Modal from 'react-native-modal';
import TimesSvg from '../../../../assets/svg/cancel.svg';
import StarSvg from '../../../../assets/svg/star (1).svg';

interface ContextProps {
  modal: boolean;
  handleClickModal(args: GestureResponderEvent): void;
}

interface ModalLikeState {
  avatar: string;
  nickname: string;
  username: string;
}

interface FlatListProps {
  index: number;
  item: ModalLikeState;
}

const FakeArray: any[] = [];

for (let i = 0; i < 30; i++) {
  const x = {
    avatar: 'https://photo.kontan.co.id/photo/2020/07/05/1916951498p.jpg',
    nickname: 'Jhon',
    username: 'Jhon X+F',
  };
  const y = {
    avatar:
      'https://www.greenscene.co.id/wp-content/uploads/2020/03/One-Piece-14-696x497.jpg',
    nickname: 'Luffi',
    username: 'Luffi Umael',
  };
  if (i % 2) {
    FakeArray.push(y);
  } else {
    FakeArray.push(x);
  }
}

export const ModalContextLikes = React.createContext<ContextProps>({});
export const ModalContextLikesApp = () => {
  function renderItem({index, item}: FlatListProps) {
    return (
      <TouchableOpacity
        key={index}
        style={{
          marginBottom: 5,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <View
          style={{
            width: 50,
            height: 50,
          }}>
          <Image
            source={{uri: item.avatar}}
            style={{flex: 1, borderRadius: 15}}
            resizeMode="cover"
          />
        </View>
        <View
          style={{
            flexDirection: 'column',
            marginLeft: 10,
            flex: 1,
          }}>
          <Text
            style={{
              color: 'black',
              fontWeight: 'bold',
            }}>
            {item.nickname}
          </Text>
          <Text
            style={{
              color: '#8e8e8e',
            }}>
            {item.username}
          </Text>
        </View>
        <View>
          <StarSvg width={30} height={30} />
        </View>
      </TouchableOpacity>
    );
  }
  return (
    <ModalContextLikes.Consumer>
      {({modal, handleClickModal}) => {
        return (
          <Modal isVisible={modal}>
            <View
              style={{
                backgroundColor: 'white',
                flex: 1,
                padding: 10,
                borderRadius: 15,
                elevation: 4,
              }}>
              <View
                style={{
                  backgroundColor: '#57c292',
                  elevation: 4,
                  padding: 10,
                  borderRadius: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginBottom: 10,
                }}>
                <TimesSvg width={30} height={30} fill="#57c292" />
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 18,
                    color: 'white',
                  }}>
                  Likes you
                </Text>
                <TouchableOpacity onPress={handleClickModal}>
                  <TimesSvg width={30} height={30} fill="white" />
                </TouchableOpacity>
              </View>
              <FlatList
                data={FakeArray}
                renderItem={renderItem}
                keyExtractor={(item) => item.nickname}
              />
            </View>
          </Modal>
        );
      }}
    </ModalContextLikes.Consumer>
  );
};
