import React from 'react';
import {GestureResponderEvent, SafeAreaView, ScrollView} from 'react-native';
import { NavigationProp } from '../../../routes';
import {AuthInter} from '../types/interfaceUser';
import AuthComponent from './component/auth.component';
import {
  SwitchContext,
  SwitchContextApp,
} from './component/context/SwitchContext';
import HeaderComponent from './component/header.component';
import {ImageUpload, ImageUploadApp} from './context/image.upload';

export type ScreenProfile = 'Intro' | 'Galery';

const ProfileComponent: React.FC<NavigationProp> = (props: React.PropsWithChildren<NavigationProp>) => {
  const [screen, setScreen] = React.useState<ScreenProfile>('Intro');
  const [state, setState] = React.useState<AuthInter>({});
  const [imageUpload, setImageUpload] = React.useState<boolean>(false);
  const handleClickChangeAvatar = (args: GestureResponderEvent) => {
    args.preventDefault();
    setImageUpload(!imageUpload);
  };
  const handleClickSwitchScreen = (args: ScreenProfile) => {
    setScreen(args);
  };
  return (
    <SwitchContext.Provider
      value={{
        s: screen,
        navigation: props
      }}>
      <ImageUpload.Provider
        value={{
          imageUpload,
          setImageUpload,
          handleClickChangeAvatar,
          state,
          setState,
        }}>
        <SafeAreaView
          style={{
            backgroundColor: 'white',
            flex: 1,
          }}>
          <ScrollView>
            <HeaderComponent
              state={state}
              handleClickChangeAvatar={handleClickChangeAvatar}
            />
            <AuthComponent handleClickSwitchScreen={handleClickSwitchScreen} />
            <SwitchContextApp />
            <ImageUploadApp />
          </ScrollView>
        </SafeAreaView>
      </ImageUpload.Provider>
    </SwitchContext.Provider>
  );
};

export default ProfileComponent;
