export interface UserInter {
  username?: string;
  email?: string;
  token?: string;
  password?: string;
  confirm_password?: string;
  rememmber?: boolean;
  lock?: boolean;
}

export interface AuthInter {
  avatar?: any;
  phone_number?: string;
  country?: string;
  states?: string;
  city?: string;
  user?: UserInter;
}
