import {Button, Text, View} from 'native-base';
import React from 'react';
import {ScreenName} from '..';
import UserSVG from '../../../assets/svg/user.svg';
import LockSvg from '../../../assets/svg/padlock.svg';
import SeeSvg from '../../../assets/svg/view.svg';
import SeeSlashSvg from '../../../assets/svg/invisible.svg';
import GoogleSvg from '../../../assets/svg/search.svg';
import FacebookSvg from '../../../assets/svg/facebook.svg';
import SearchSvg from '../../../assets/svg/loupe.svg';
import {
  Animated,
  Dimensions,
  GestureResponderEvent,
  NativeSyntheticEvent,
  StyleSheet,
  TextInput,
  TextInputChangeEventData,
  TouchableOpacity,
} from 'react-native';
import {UserInter} from '../types/interfaceUser';
import { AuthenticateContext } from '../../../routes/AuthorizationContext';

interface ContextProps {
  screen?: ScreenName;
  handleClickScreen?(args: ScreenName): void;
}

export const ContextAuthComponent = React.createContext<ContextProps>({});

export const ContextAuthComponentApp = () => {
  return (
    <ContextAuthComponent.Consumer>
      {({screen, handleClickScreen}) => {
        if (screen === 'login') {
          return (
            <LoginComponent
              screen={screen}
              handleClickScreen={handleClickScreen}
            />
          );
        } else if (screen === 'register') {
          return (
            <RegisterComponent
              screen={screen}
              handleClickScreen={handleClickScreen}
            />
          );
        } else if (screen === 'forgot') {
          return (
            <ForgotComponent
              screen={screen}
              handleClickScreen={handleClickScreen}
            />
          );
        }
      }}
    </ContextAuthComponent.Consumer>
  );
};

const AuthStyleForm = StyleSheet.create({
  root: {flex: 1, justifyContent: 'space-between'},
  header: {
    backgroundColor: '#57c292',
    height: 120,
    borderBottomLeftRadius: 100,
    justifyContent: 'center',
  },
  textHeader: {
    fontSize: 40,
    fontWeight: 'bold',
    textAlign: 'center',
    textTransform: 'capitalize',
    color: 'white',
  },
  field: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 10,
  },
  iconx: {
    backgroundColor: 'white',
    width: 40,
    height: 40,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 4,
  },
  input: {
    width: 300,
    borderWidth: 2.5,
    borderTopColor: 'transparent',
    borderRightColor: 'transparent',
    borderLeftColor: 'transparent',
    borderBottomRightRadius: 15,
    marginLeft: 10,
    borderBottomColor: '#57c292',
    fontWeight: 'bold',
  },
  button: {
    width: Dimensions.get('screen').width / 2,
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 20,
    backgroundColor: 'white',
    elevation: 4,
    borderColor: '#57c292',
    borderWidth: 3,
  },
  textButton: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'black',
    textTransform: 'capitalize',
  },
  provide: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 15,
  },
  textProvide: {
    color: 'black',
    fontSize: 17,
    fontWeight: 'bold',
    textTransform: 'capitalize',
  },
  groupProviderAuth: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 15,
  },
  providerAuth: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingTop: 5,
    paddingBottom: 5,
    width: 150,
    justifyContent: 'center',
    elevation: 4,
    borderRadius: 20,
    borderWidth: 3,
    borderColor: '#57c292',
  },
  textProviderAuth: {
    fontSize: 17,
    fontWeight: 'bold',
    marginLeft: 10,
  },
  optionsProvider: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  copyRight: {
    textAlign: 'center',
    fontWeight: 'bold',
    textTransform: 'capitalize',
    marginBottom: 5,
  },

  // Another;
  headerRecord: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 180,
    backgroundColor: '#57c292',
    borderBottomLeftRadius: 80,
  },
  textChildHeaderRecord: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
  },
  textHeaderRecord: {
    fontSize: 35,
    fontWeight: 'bold',
    color: 'white',
    marginLeft: 10,
    textTransform: 'capitalize',
  },
  singleProvider: {
    fontSize: 17,
    fontWeight: 'bold',
    textAlign: 'center',
    textTransform: 'capitalize',
    marginTop: 15,
  },
});

const LoginComponent: React.FC<ContextProps> = ({
  screen,
  handleClickScreen,
}: React.PropsWithChildren<ContextProps>) => {
  const [state, setState] = React.useState<UserInter>({lock: true});
  const { SignIn } = React.useContext(AuthenticateContext)
  const onChangeUsername = (
    args: NativeSyntheticEvent<TextInputChangeEventData>,
  ) => {
    setState({
      ...state,
      username: args.nativeEvent.text,
    });
  };
  const onChangePassword = (
    args: NativeSyntheticEvent<TextInputChangeEventData>,
  ) => {
    setState({
      ...state,
      password: args.nativeEvent.text,
    });
  };
  const handleClickLock = (args: GestureResponderEvent) => {
    args.preventDefault();
    setState({
      ...state,
      lock: !state.lock,
    });
  };
  const onPress = () => {
    SignIn(state)
  }
  return (
    <View style={AuthStyleForm.root}>
      <View style={AuthStyleForm.header}>
        <Text style={AuthStyleForm.textHeader}>mosquito</Text>
      </View>
      <View>
        <View style={AuthStyleForm.groupProviderAuth}>
          <TouchableOpacity
            style={{...AuthStyleForm.providerAuth, marginRight: 8}}>
            <GoogleSvg width={30} height={30} />
            <Text style={AuthStyleForm.textProviderAuth}>Google</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{...AuthStyleForm.providerAuth, marginLeft: 8}}>
            <FacebookSvg width={30} height={30} />
            <Text style={AuthStyleForm.textProviderAuth}>Facebook</Text>
          </TouchableOpacity>
        </View>
        <Text style={AuthStyleForm.optionsProvider}>Or Log in With</Text>
        <View style={AuthStyleForm.field}>
          <View style={AuthStyleForm.iconx}>
            <UserSVG width={25} height={25} fill="black" />
          </View>
          <TextInput
            value={state.username}
            onChange={onChangeUsername}
            placeholder="Username"
            style={AuthStyleForm.input}
            placeholderTextColor="black"
          />
        </View>
        <View style={AuthStyleForm.field}>
          <View style={AuthStyleForm.iconx}>
            <LockSvg width={25} height={25} fill="black" />
          </View>
          <TextInput
            value={state.password}
            onChange={onChangePassword}
            placeholder="Password"
            secureTextEntry={state.lock}
            style={{...AuthStyleForm.input, width: 279}}
            placeholderTextColor="black"
          />
          <TouchableOpacity onPress={handleClickLock}>
            {!state.lock ? (
              <SeeSvg width={25} height={25} fill="black" />
            ) : (
              <SeeSlashSvg width={25} height={25} fill="black" />
            )}
          </TouchableOpacity>
        </View>
        <Button style={AuthStyleForm.button} onPress={onPress}>
          <Text style={AuthStyleForm.textButton}>Sign in</Text>
        </Button>
        <View style={AuthStyleForm.provide}>
          <TouchableOpacity onPress={handleClickScreen?.bind('', 'register')}>
            <Text style={AuthStyleForm.textProvide}>create new accounts?</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={handleClickScreen?.bind('', 'forgot')}>
            <Text style={AuthStyleForm.textProvide}>forgotted password?</Text>
          </TouchableOpacity>
        </View>
      </View>
      <Text style={AuthStyleForm.copyRight}>© 2021 mosquito, Inc.</Text>
    </View>
  );
};

const RegisterComponent: React.FC<ContextProps> = ({
  screen,
  handleClickScreen,
}: React.PropsWithChildren<ContextProps>) => {
  const [state, setState] = React.useState<UserInter>({
    lock: true,
  });
  const handleClickLock = (args: GestureResponderEvent) => {
    args.preventDefault();
    setState({
      ...state,
      lock: !state.lock,
    });
  };
  return (
    <View style={AuthStyleForm.root}>
      <View style={AuthStyleForm.headerRecord}>
        <Text style={AuthStyleForm.textChildHeaderRecord}>Join to</Text>
        <Text style={AuthStyleForm.textHeaderRecord}>mosquito</Text>
      </View>
      <View>
        <View style={AuthStyleForm.field}>
          <View style={AuthStyleForm.iconx}>
            <UserSVG width={25} height={25} fill="black" />
          </View>
          <TextInput
            placeholder="Email"
            style={AuthStyleForm.input}
            placeholderTextColor="black"
          />
        </View>
        <View style={AuthStyleForm.field}>
          <View style={AuthStyleForm.iconx}>
            <LockSvg width={25} height={25} fill="black" />
          </View>
          <TextInput
            placeholder="Password"
            secureTextEntry={state.lock}
            style={AuthStyleForm.input}
            placeholderTextColor="black"
          />
        </View>
        <View style={AuthStyleForm.field}>
          <View style={AuthStyleForm.iconx}>
            <LockSvg width={25} height={25} fill="black" />
          </View>
          <TextInput
            placeholder="Password"
            secureTextEntry={state.lock}
            style={{...AuthStyleForm.input, width: 279}}
            placeholderTextColor="black"
          />
                    <TouchableOpacity onPress={handleClickLock}>
            {!state.lock ? (
              <SeeSvg width={25} height={25} fill="black" />
            ) : (
              <SeeSlashSvg width={25} height={25} fill="black" />
            )}
          </TouchableOpacity>
        </View>
        <Button style={AuthStyleForm.button}>
          <Text style={AuthStyleForm.textButton}>Sign up</Text>
        </Button>
        <TouchableOpacity onPress={handleClickScreen?.bind('', 'login')}>
          <Text style={AuthStyleForm.singleProvider}>
            already exists accounts?
          </Text>
        </TouchableOpacity>
      </View>
      <Text style={AuthStyleForm.copyRight}>© 2021 mosquito, Inc.</Text>
    </View>
  );
};

const ForgotComponent: React.FC<ContextProps> = ({
  screen,
  handleClickScreen,
}: React.PropsWithChildren<ContextProps>) => {
  return (
    <View style={AuthStyleForm.root}>
      <View style={{...AuthStyleForm.headerRecord, flexDirection: 'column'}}>
        <Text style={AuthStyleForm.textHeader}>Reset Password</Text>
        <Text
          style={{
            ...AuthStyleForm.textChildHeaderRecord,
            marginRight: 30,
            marginLeft: 30,
            textAlign: 'center',
          }}>
          Enter your user account's verified email address and we will send you
          a password reset link.
        </Text>
      </View>
      <View>
        <View style={AuthStyleForm.field}>
          <View style={AuthStyleForm.iconx}>
            <SearchSvg width={22} height={22} fill="black" />
          </View>
          <TextInput
            placeholder="Find username, email or phone number"
            style={AuthStyleForm.input}
            placeholderTextColor="black"
          />
        </View>
        <Button style={AuthStyleForm.button}>
          <Text style={AuthStyleForm.textButton}>Find</Text>
        </Button>
        <TouchableOpacity onPress={handleClickScreen?.bind('', 'login')}>
          <Text style={AuthStyleForm.singleProvider}>
            already exists accounts?
          </Text>
        </TouchableOpacity>
      </View>
      <Text style={AuthStyleForm.copyRight}>© 2021 mosquito, Inc.</Text>
    </View>
  );
};
