import React from 'react';
import {SafeAreaView} from 'react-native';
import {NavigationProp} from '../../routes';
import {ContextAuthComponent, ContextAuthComponentApp} from './context';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export type ScreenName = 'login' | 'register' | 'forgot';

export default ({navigation}: React.PropsWithChildren<NavigationProp>) => {
  const [screen, setScreen] = React.useState<ScreenName>('login');
  const handleClickScreen = (args: ScreenName) => {
    setScreen(args);
  };
  return (
    <SafeAreaView style={{flexGrow: 1, backgroundColor: 'white'}}>
      <KeyboardAwareScrollView
        contentContainerStyle={{flexGrow: 1}}
        enableOnAndroid={true}>
        <ContextAuthComponent.Provider
          value={{
            screen,
            handleClickScreen,
          }}>
          <ContextAuthComponentApp />
        </ContextAuthComponent.Provider>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};
