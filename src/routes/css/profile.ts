import {StyleSheet} from 'react-native'

export const ProfileStyle = StyleSheet.create({
    root: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    textButton: {},
    buttonRight: {
      marginRight: 5,
      flexDirection: 'row',
      alignItems: 'center',
    },
    buttonLeft: {
      marginLeft: 5,
      marginRight: 5,
      flexDirection: 'row',
      alignItems: 'center',
    },
    text: {
      color: 'white',
      fontWeight: 'bold',
      fontSize: 18,
    },
    childView: {
      marginLeft: 10,
      width: 60,
      height: 30,
      backgroundColor: 'white',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 10,
      elevation: 4,
    },
    textView: {
      color: 'black',
      fontSize: 12,
      fontWeight: 'bold',
    },
  });