import { StyleSheet } from 'react-native'

export const headerTitleStyle = StyleSheet.create({
    headerTitleStyle: {
        textTransform: 'capitalize',
        alignSelf: 'center',
        color: 'white',
        fontSize: 28,
      }
})