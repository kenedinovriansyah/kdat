import React from 'react';
import {
  createStackNavigator,
  StackNavigationProp,
} from '@react-navigation/stack';
import {
  NavigationContainer,
  DefaultTheme,
  RouteProp,
} from '@react-navigation/native';
import AuthScreen from '../screen/auth';
import {User} from '../redux/types/interface';
import {useDispatch, useSelector} from 'react-redux';
import {AuthenticateContext} from './AuthorizationContext';
import {ApplicationState} from '../redux/configureStore';
import {userAccess} from '../redux/actions/userActions';
import {TabDating} from './tabbar/Tdating';
import {UserTypes} from '../redux/types/userTypes';
import {Text, View} from 'native-base';
import ProfileComponent from '../screen/auth/profile';
import {TouchableOpacity} from 'react-native';
import {ProfileStyle} from './css/profile';
import {headerTitleStyle} from './css/dating';
import ArrowLeft from '../assets/svg/left-arrow.svg';
import DetailImage from '../screen/auth/profile/detail/index.component';

export type RouteName = 'auth' | 'room' | 'profile';

export type RootStackState = {
  auth: undefined;
  room: undefined;
  profile: undefined;
  detailImage: {uri: string};
};

const RootStack = createStackNavigator<RootStackState>();

type RootStateProps = StackNavigationProp<RootStackState>;
type RouteProps = RouteProp<RootStackState, 'detailImage'>;

export interface NavigationProp {
  navigation: RootStateProps;
  route: RouteProps;
}

const Themes = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: 'rgb(255, 45, 85)',
  },
};

const Routes: React.FC = () => {
  const dispatch = useDispatch();
  const selector = useSelector((states: ApplicationState) => states.user.token);
  const AuthContext = React.useMemo(
    () => ({
      SignIn: async (args: User) => {
        // await dispatch(userAccess(args));
        await dispatch({
          type: UserTypes.SignIn,
          payload: {
            token: 'Access',
          },
        });
      },
    }),
    [],
  );
  return (
    <AuthenticateContext.Provider value={AuthContext}>
      {selector ? (
        <NavigationContainer>
          <RootStack.Navigator initialRouteName="room">
            <RootStack.Screen
              name="room"
              options={{
                headerTitle: 'mosquito',
                headerTitleStyle: headerTitleStyle.headerTitleStyle,
                headerShown: true,
                headerStyle: {
                  backgroundColor: '#57c292',
                },
              }}
              component={TabDating}></RootStack.Screen>
            <RootStack.Screen
              name="profile"
              component={ProfileComponent}
              options={{
                headerLeft: ({onPress}) => {
                  return (
                    <TouchableOpacity
                      style={{
                        marginLeft: 10,
                      }}
                      onPress={onPress}>
                      <ArrowLeft width={20} height={20} fill="white" />
                    </TouchableOpacity>
                  );
                },
                headerRight: (props) => (
                  <View style={ProfileStyle.root}>
                    <TouchableOpacity style={ProfileStyle.buttonRight}>
                      <Text style={ProfileStyle.text}>Follow</Text>
                      <View style={ProfileStyle.childView}>
                        <Text style={ProfileStyle.textView}>20</Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={ProfileStyle.buttonLeft}>
                      <Text style={ProfileStyle.text}>Followers</Text>
                      <View style={ProfileStyle.childView}>
                        <Text style={ProfileStyle.textView}>20</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                ),
                headerTitle: '',
                headerStyle: {
                  backgroundColor: '#57c292',
                },
              }}
            />
            <RootStack.Screen name="detailImage" component={DetailImage} options={{
              headerShown: false
            }}/>
          </RootStack.Navigator>
        </NavigationContainer>
      ) : (
        <NavigationContainer theme={Themes}>
          <RootStack.Navigator initialRouteName="auth">
            <RootStack.Screen
              name="auth"
              component={AuthScreen}
              options={{
                headerShown: false,
              }}></RootStack.Screen>
          </RootStack.Navigator>
        </NavigationContainer>
      )}
    </AuthenticateContext.Provider>
  );
};

export default Routes;
