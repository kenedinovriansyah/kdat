import React from 'react';
import {
  BottomTabBarProps,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import {RootStackState, RouteName} from '..';
import TemplateComponent from '../../screen/template';
import {Button, Text, View} from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome5';
import {FlatList, StyleSheet, TouchableOpacity} from 'react-native';

const RootStack = createBottomTabNavigator<RootStackState>();

type BottomName = 'Home' | 'Search' | 'Profil' | 'Interest' | 'Upload';

interface BottomState {
  name: BottomName;
  icon: React.ReactElement<FontAwesome>;
  router: RouteName;
}

const bottomArray: BottomState[] = [
  {
    name: 'Home',
    icon: <FontAwesome name="home" size={25} color="white" />,
    router: 'profile',
  },
  {
    name: 'Search',
    icon: <FontAwesome name="search" size={25} color="white" />,
    router: 'profile',
  },
  {
    name: 'Upload',
    icon: <FontAwesome name="plus" size={25} color="white" />,
    router: 'profile',
  },
  {
    name: 'Interest',
    icon: <FontAwesome name="heart" size={25} color="white" />,
    router: 'profile',
  },
  {
    name: 'Profil',
    icon: <FontAwesome name="user" size={25} color="white" />,
    router: 'profile',
  },
];

interface FlatListProps {
  index: number;
  item: BottomState;
}

const TdatingStyle = StyleSheet.create({});

const CustomTab = ({navigation}: BottomTabBarProps) => {
  const handleClickRouter = (args: RouteName) => {
    navigation.navigate(args);
  };
  function renderItem({item, index}: FlatListProps) {
    return (
      <TouchableOpacity
        key={index}
        style={{
          flexDirection: 'column',
          alignItems: 'center',
        }}
        onPress={handleClickRouter.bind(item, item.router)}>
        {item.icon}
        <Text
          style={{
            fontSize: 14,
            fontWeight: 'bold',
            color: 'white',
            textTransform: 'capitalize',
          }}>
          {item.name}
        </Text>
      </TouchableOpacity>
    );
  }
  return (
    <View>
      <FlatList
        contentContainerStyle={{
          flexDirection: 'row',
          alignItems: 'center',
          backgroundColor: '#57c292',
          height: 60,
          justifyContent: 'space-around',
          elevation: 4,
        }}
        data={bottomArray}
        renderItem={renderItem}
        keyExtractor={(item) => item.name}
      />
    </View>
  );
};

export const TabDating = () => {
  return (
    <RootStack.Navigator tabBar={(props) => <CustomTab {...props} />}>
      <RootStack.Screen name="room" component={TemplateComponent} />
    </RootStack.Navigator>
  );
};

export default TabDating;
