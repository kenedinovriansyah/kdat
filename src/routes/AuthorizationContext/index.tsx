import React from 'react';
import {User} from '../../redux/types/interface';

interface ContextProps {
  SignIn(args: User): void;
}

export const AuthenticateContext = React.createContext<ContextProps>({
  SignIn: async (args: User) => {
    return 0;
  },
});
